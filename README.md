# GitLab Orchestrator

## Overview

The main goal for GitLab Orchestrator is to provide an automated, repeatable
method for installing GitLab Omnibus and its related services into a cluster
of machines.

The current state is tracked in the [Automate deployment of Geo+HA epic](https://gitlab.com/groups/gitlab-org/-/epics/2376).

## Original Proposal

The chart below is taken from [the initial proposal to make the
`gitlab-provisioner` more extensible](https://gitlab.com/gitlab-org/gitlab-orchestrator/issues/3).
The linked issue provides a large amount of detail about the rationale for
this architectural design.

```mermaid
graph LR;
  tf -->|provisioning request|cloudapi

  subgraph "Orchestration"
    orch[ansible] -->|query hosts|tfinv[Inventory Parser]
  end

  subgraph "Cloud Provider"
    cloudapi[cloud service provider] -->|request creation|cluster[provisioned nodes]
  end

  subgraph "Provisioned State"
    tfinv[Inventory Parser] -->|read state|provstate
    provstate[node state data]
  end

  subgraph "Provisioner"
    tf[terraform] -->|transmit state data|tfstate[terraform state data store]
    tf -->|write state|provstate
  end
```

## Minimum Start

The minimum start is to provide support for the Geo team as customer zero.
Getting to that starting goal requires several things to happen:

- Extract scripts to allow pipelines and human run tests to be equivalent
- Extend tagging to include Geo resources in the terraform setup
- Codify a minimal Geo playbook

## Workflow overview

The chart below illustrates the expected workflow for the early version of
the orchestrator:

```mermaid
stateDiagram
  state Initialization {
      GetToken --> ConfigureSecrets
      ConfigureSecrets --> BuildContainer
      BuildContainer --> RunContainer
  }
  note right of Initialization
    - Secrets placed in secrets/user_config.sh
    - scripts/build-dev-container.sh
    - scripts/run-dev-container.sh
  end note
  state Provisioning {
      TerraformCreate --> ExtractInventory
      ExtractInventory --> ExtraVarsCreate
  }
  note right of Provisioning : provision-cluster
  state InventoryAssembly {
      GenerateNodeInventory --> IdentifySSHKeys
  }
note right of InventoryAssembly : assemble-inventory
  state Orchestrating {
      AnsibleRun
  }
  note right of Orchestrating : run-orchestration
  state Deprovisioning {
      TerraformDestroy --> CleanUpLocal
  }
  state Exit {
      ExitContainer
  }
  note right of Deprovisioning : deprovision-cluster
  Initialization --> Provisioning
  Provisioning --> InventoryAssembly
  InventoryAssembly --> Orchestrating
  Orchestrating --> Deprovisioning
  Deprovisioning --> Exit
```
