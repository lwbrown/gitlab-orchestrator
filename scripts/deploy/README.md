## Requirements

The following packages must be installed for these scripts to run:

- [Ruby 2.6.x or later](https://www.ruby-lang.org/en/)
- [jq](https://stedolan.github.io/jq/)
- [Ansible](https://www.ansible.com/)
- [Terraform](https://www.terraform.io/)

## Cluster Deployment Scripts

### Running the Deployment

- `provisioner-cluster` creates a cluster of GCP machines
- `assemble-inventory` gathers all inventory information from the cluster
- `run-orchestration` executes the Ansible playbooks
- `deprovision-cluster` destroys the cluster of GCP machines

### Deployment Support Scripts

- `fetch-cluster-logs` gathers logs from the cluster
- `validate-provisioner` will ensure the terraform scripts are OK

## Configuration for the Developer Container Environment

Create an **rc** file to control the developer container environment. This
will replicate the pipeline variables from continuous integration.

The deploy scripts look for `${SECRETS_PATH}/gitlab-cluster-rc.sh` by default.
Configure multiple **rc** files and pass them to the deploy scripts using
the `-c` option for more flexibility.

| Variable | Description | Default | Example | Required |
| -------- | ----------- | ------- | ------- | -------- |
| `GOOGLE_JSON_FILE` | Name of the JSON file containing google credentials. | google-credentials.json | my-user-creds.json | Yes |
| `TERRAFORM_PREFIX` | Prefix labels attached to provisioned cluster resources. | *None* | my-unique-prefix | Yes |
| `STATE_GCS_BUCKET` | The bucket name in Google storage. | *None* | my-bucket | Yes |
| `GCS_PREFIX` | The directory name under the defined storage bucket. | *None* | my-state-storage-dir | Yes |
| `GCLOUD_ZONE` | The name of the specific Google Cloud zone to deploy. | *None* | us-east1-b | Yes |
| `GCLOUD_PROJECT` | The project name in Google Cloud. | *None* | my-project-a | Only if not present in `GOOGLE_JSON_FILE` |
| `SSH_USER` | The name of the user Ansible will use to connect and configure nodes. | ansible | my-username | Yes |
| `SSH_KEY_NAME` | The file name of the private key for connecting to the cluster. | gitlab-orchestrator | No |
| `PACKAGE_URL` | URL pointing at a GitLab package to install. | *None* | https://path.to.my.package | No |
| `PACKAGE_REPOSITORY` | Opt to use either the Enterprise Edition or Community Edition in a repository. | *None* | `ee` or `ce` | No |
| `PACKAGE_REPOSITORY_URL`| URL to alternative packagecloud repository. | Points to the new repository defined in `PACKAGE_REPOSITORY`.|
| `GL_PRIVATE_TOKEN` | The GitLab Token that will be used to download packages from GitLab artifacts | *None* | my-token | No |
| `postgresql_gitlab_consul_user_password` | Password for gitlab-consul user in postgres database | *None* | my-secret-pass | Yes |
| `postgresql_pgbouncer_user_password` | Password for pgbouncer user in postgres database | *None* | my-secret-pass | Yes |
| `postgresql_sql_user_password` | General postgres database sql password | *None* | my-secret-pass | Yes |
| `ANSIBLE_BECOME_METHOD` | Method Ansible will use to escalate privileges. | sudo | `sudo` | No |
