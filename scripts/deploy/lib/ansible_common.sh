# shellcheck shell=bash

# Environment Config
export ANSIBLE_FORCE_COLOR=true
export ANSIBLE_BECOME_METHOD="sudo"
export ANSIBLE_INVENTORY_FILE="${SRC_PATH}/base.gcp.yml"
export ANSIBLE_EXTRA_VARS_FILE="${SRC_PATH}/extra_vars.json"

# Load SSH Key for use by Ansible
eval "$(ssh-agent -s)"
echo "${SSH_PRIVATE_KEY}" | tr -d '\r' | ssh-add - > /dev/null

