# shellcheck shell=bash

# Common Defines and Libraries
# ------------------------------------------------------------------------------
# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/lib/helpers.sh"

# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/lib/src-paths.sh"

# Configuration Information
# ------------------------------------------------------------------------------
export GITLAB_CLUSTER_RC="${GITLAB_CLUSTER_RC:-${SECRETS_PATH}/gitlab-cluster-rc.sh}"

if [ -f "${GITLAB_CLUSTER_RC}" ]; then
    echo "Loading configuration from ${GITLAB_CLUSTER_RC}"
    set -a
    . "${GITLAB_CLUSTER_RC}"
    set +a

    . "${SRC_PATH}/scripts/deploy/lib/rc-helpers.sh"

    SSH_KNOWN_HOSTS_FILE="${SSH_KNOWN_HOSTS_FILE:-${HOME}/.ssh/known_hosts}"
    mkdir -p "$(dirname "${SSH_KNOWN_HOSTS_FILE}")"

    GOOGLE_JSON_FILE="${GOOGLE_JSON_FILE:-google-credentials.json}"
    GOOGLE_APPLICATION_CREDENTIALS="${SECRETS_PATH}/${GOOGLE_JSON_FILE}"

    private_key="$(select_ssh_key "${SSH_KEY_NAME}")"
    public_key="${private_key}.pub"

    SSH_PRIVATE_KEY="$(cat "${private_key}")"
    SSH_PUBLIC_KEY="$(cat "${public_key}")"
    export SSH_PRIVATE_KEY
    export SSH_PUBLIC_KEY

    SSH_USER="${SSH_USER:-ansible}"

    # The following items are REQUIRED in an rc file and have no defaults
    validate_variable "postgresql_gitlab_consul_user_password"
    validate_variable "postgresql_pgbouncer_user_password"
    validate_variable "postgresql_sql_user_password"
    validate_variable "TERRAFORM_PREFIX"
    validate_variable "GCS_PREFIX"
    validate_variable "STATE_GCS_BUCKET"
    validate_variable "GCLOUD_ZONE"

    validate_package_source "${package_url:=}" "${package_repository:=}" "${package_repository_url:=}" "${GL_PRIVATE_TOKEN:=}"
fi

# General Configuration
# ------------------------------------------------------------------------------
export ORCHESTRATION_USER="${SSH_USER:-ansible}"
export QA_IP_FILE="${QA_IP_FILE:-${SRC_PATH}/rc_gitlab_application_public_ip_address}"
export SSH_KNOWN_HOSTS_FILE="${SSH_KNOWN_HOSTS_FILE:-${HOME}/.ssh/known_hosts}"

# Google Cloud Credentials
# ------------------------------------------------------------------------------
export GOOGLE_APPLICATION_CREDENTIALS="${GOOGLE_APPLICATION_CREDENTIALS:-${SRC_PATH}/google-credentials.json}"

if [ -n "${GCLOUD_CREDENTIALS_JSON}" ]; then
    printf "%s" "${GCLOUD_CREDENTIALS_JSON}" > "${GOOGLE_APPLICATION_CREDENTIALS}"
fi

if [ -z "${GCLOUD_PROJECT}" ]; then
    GCLOUD_PROJECT=$(jq -r .project_id < "${GOOGLE_APPLICATION_CREDENTIALS}")
fi
export GCLOUD_PROJECT
