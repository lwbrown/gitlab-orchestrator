# shellcheck shell=bash

ANSIBLE_PATH="${ANSIBLE_PATH:-${SRC_PATH}/ansible}"
TERRAFORM_PATH="${TERRAFORM_PATH:-${SRC_PATH}/terraform}"
SECRETS_PATH="${SECRETS_PATH:-${SRC_PATH}/secrets}"
SCRIPTS_PATH="${SCRIPTS_PATH:-${SRC_PATH}/scripts}"
DEPLOY_TEMPLATES="${DEPLOY_TEMPLATES:-${SCRIPTS_PATH}/deploy/templates}"
CI_PATH="${CI_PATH:-${SRC_PATH}/ci}"

# Purposefully not using SRC_PATH because when used at container runtime,
# they use one path and when inside the container they use another
export HOST_SSH_KEY_DIR="host-ssh-keys"
export MOUNTED_SSH_KEY_DIR="mounted-ssh-keys"
