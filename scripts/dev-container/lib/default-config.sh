# shellcheck shell=bash

# Configuration Outside the Container
export CONTAINER_VERSION="v1"
export DOCKERFILE="${SRC_PATH}/Dockerfile.dev"
export CONTEXT="${SRC_PATH}"

# Configuration Inside the Container
export CONTAINER_SRC_PATH="/repo"
