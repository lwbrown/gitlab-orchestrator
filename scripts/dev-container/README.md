## Building the Developer Container Environment

## Quickstart

```sh
./scripts/dev-container/build-dev-container
./scripts/dev-container/run-dev-container
```

## Configuring the Build and Resulting Environment

The build and deployment environment is controlled through environment
variables that may be set in [default](./README.md#default-secrets-locations) or
[user specified](./README.md#specifying-secrets-locations) locations.

### Default Secrets Locations

The developer environment assumes that a directory named `secrets` will be
created at the top directory of the repository. This is where Google
credentials and the [**gitlab-cluster-rc**](../deploy/README.md#configuration-for-the-developer-container-environment) for deploy scripts should live.

The deploy tools will generate a default SSH keypair if none is provided.

### Specifying Secrets Locations

Configure the build and run scripts by creating the
`${HOME}/.gitlab-orchestrator-rc` file. Set none or any of the following to
control the secrets injected into the developer container environment.

```sh
SECRETS_PATH=/path/to/directory/containing/credentials/and/rcs
SSH_PATH=/path/to/directory/containing/ssh/keypairs
PRIVATE_KEY_PATHS=(/path/to/private_key1 /path/to/private_key2)
```

Note that using `SSH_PATH` will make *all* contents of the specified
directory available to the developer's container. If this is not desirable,
then specify the path to the private key of one or many SSH keypairs using
`PRIVATE_KEY_PATHS` and each will be mounted into the container.

The container environment will look for keys specified using the
following priority order:

- `PRIVATE_KEY_PATHS` or `-k` options to `run-dev-container`
- `SSH_PATH`
- Dynamically generated ssh key

Each of the above options may also be set when the script runs, consult
`./scripts/dev-container/run-dev-container -h` for more information.
