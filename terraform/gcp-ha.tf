terraform {
  backend "gcs" {}
}

variable "consul_count" {
  description = "Number of consul nodes to create"
  default     = 3
}

variable "database_count" {
  description = "Number of database nodes to create"
  default     = 3
}

variable "application_count" {
  description = "Number of application nodes to create"
  default     = 1
}

variable "prefix" {
  description = "Namespace resources will be created with.  Example: rclamp will create rclamp-consul-0"
}

variable "ssh_user" {
  description = "Username to allow ssh in with"
}

variable "ssh_public_key" {
  description = "ssh public_key to use for ssh_user"
}

variable "google_project" {
  description = "Google Project ID"
}

variable "google_zone" {
  description = "Google zone to provision resources in"
  default     = "us-central1-b"
}

locals {
  google_region = join("-", slice(split("-", var.google_zone), 0, 2))
}

provider "google" {
  project = var.google_project
  region  = local.google_region
  zone    = var.google_zone
}

resource "google_compute_instance" "consul" {
  count = var.consul_count
  name  = "${var.prefix}-consul-${count.index}"

  tags = ["consul", var.prefix]

  labels = {
    ansible_group = "${var.prefix}-consul"
    cluster_group = "${var.prefix}-gcp-cluster"
  }

  metadata = {
    ssh-keys = "${var.ssh_user}:${var.ssh_public_key}"
  }

  machine_type = "g1-small"

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1604-lts"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }
}

output "consul-machine-names" {
  value = google_compute_instance.consul[*].name
  description = "Consul node hostname that will be used in the resulting Ansible job"
}

output "consul-internal-addresses" {
  value = google_compute_instance.consul[*].network_interface[0].network_ip
}

output "consul-addresses" {
  value = google_compute_instance.consul[*].network_interface[0].access_config[0].nat_ip
}

resource "google_compute_instance" "database" {
  count = var.database_count
  name  = "${var.prefix}-database-${count.index}"

  tags = ["database", var.prefix]

  labels = {
    ansible_group = "${var.prefix}-database"
    cluster_group = "${var.prefix}-gcp-cluster"
  }

  metadata = {
    ssh-keys = "${var.ssh_user}:${var.ssh_public_key}"
  }

  machine_type = "n1-standard-1"

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1604-lts"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }
}

output "database-machine-names" {
  value = google_compute_instance.database[*].name
  description = "Database node hostname that will be used in the resulting Ansible job"
}

output "database-internal-addresses" {
  value = google_compute_instance.database[*].network_interface[0].network_ip
}

output "database-addresses" {
  value = google_compute_instance.database[*].network_interface[0].access_config[0].nat_ip
}

resource "google_compute_instance" "application" {
  count = var.application_count
  name  = "${var.prefix}-application-${count.index}"

  tags = ["application", "http-server", "https-server", var.prefix]

  labels = {
    ansible_group = "${var.prefix}-application"
    cluster_group = "${var.prefix}-gcp-cluster"
  }

  machine_type = "n1-standard-1"

  metadata = {
    ssh-keys = "${var.ssh_user}:${var.ssh_public_key}foo"
  }

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1604-lts"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }
}

output "application-machine-names" {
  value = google_compute_instance.application[*].name
  description = "Application node hostname that will be used in the resulting Ansible job"
}

output "application-internal-addresses" {
  value = google_compute_instance.application[*].network_interface[0].network_ip
}

output "application-addresses" {
  value = google_compute_instance.application[*].network_interface[0].access_config[0].nat_ip
}
