# Using ansible to deploy a GitLab configuration

Using the playbooks in this repository, you can easily spin up a GitLab instance using predefined configurations

## Prerequisites

* Nodes to configure
  * For PG HA, please see [our documentation](https://docs.gitlab.com/ee/administration/high_availability/database.html#architecture) for the recommended number of nodes to provision
  * Each node will need python to be installed
  * You will require root access to the nodes
* An [Ansible installation](http://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## Populate your inventory

An example inventory
```yaml
[consul]
consul-a
consul-b
consul-c

[database]
database-a
database-b
database-c

[app]
app-a
```

## Set any necessary variables

| Variable | Purpose | Default | Example | Required |
| -------- | ------- | ------- | ------- | -------- |
| consul_bind_address | If you want consul to bind to a specific address, use that here | None | 192.168.1.100 | No |
| consul_bind_interface | If you want consul to bind to a specific interface, use that here | None | eth1 | No |
| consul_retry_join | A list of consul server nodes for the host to attempt to join on startup | Default IP addresses of the consul server nodes |  - 192.168.100.0/24<br />- 192.168.101.0/24  | No |
| expected_master | If setting up a new cluster, then specify which host should be the master | None | db-master.example.com | No |
| package_name | The name of the package to install. *Warning* You probably don't want to change this | gitlab-ee | fake-package | No |
| package_repository | The GitLab repository to use from Package Cloud | gitlab-ee | gitlab-ce | No |
| package_repository_url | An arbitrary line to add for setting up a package repository | None | deb http://example.com/debian stable main | No |
| package_url | An arbitrary url to download a package from | None | http://example.com/gitlab.deb | No |
| package_version | The version of the package to install. | Latest version | 10.3.9-ee.0 | No |
| postgresql_gitlab_consul_user_password | The database password for the gitlab-consul user | | toomanysecrets | Yes |
| postgresql_listen_address | The address postgresql should listen on | 0.0.0.0 | 127.0.0.1 | Yes |
| postgresql_pgbouncer_user_password | The database password for the pgbouncer user | None | fizzbuzz | Yes |
| postgresql_sql_user_password | The database password for the application sql user | None | fizzbuzz | Yes |
| postgresql_trust_auth_cidr_addresses | A list of IP subnets that are trusted to connect to the database | None | - 192.168.100.0/24<br />- 192.168.101.0/24 | Yes |
| max_replication_slots | Integer representing the desired number of slots. | 0 | 3 | No |
| repmgr_trust_auth_cidr_addresses | A list of IP subnets that are trusted to manage the repmgr configuration | None | 192.168.100.0/24<br />- 192.168.101.0/24 | Yes |

To set these variables, use one of the following

### In your inventory file as a host variable. I.E.

   ```yaml
   consul-a consul_bind_interface=eth1
   ```
### In the appropriate group_vars file

  ```yaml
   # in group_vars/consul
   consul_bind_interface: eth1
  ```
### As a command line argument

   ```shell
   $ ansible-playbook .... -e consul_bind_interface=eth1
   ```
### Create your own configuration file

   ```yaml
   postgresql_gitlab_consul_user_password: 'consul_password'
   postgresql_listen_address: 0.0.0.0
   pgbouncer_user_password: 'pgbouncer_password'
   sql_user_password: 'git_password'
   postgresql_trust_auth_cidr_addresses:
       - 127.0.0.0/24
       - 192.168.100.0/24
   repmgr_trust_auth_cidr_addresses:
       - 192.168.100.0/24
   ```

   ```shell
   $ ansible-playbook ... -e @config.yml
   ```

## Configure privilege escalation

By default, ansible connects to the nodes as the current user, and will attempt to use `sudo` for privilege escalation when required.

If your account requires a password to use sudo, you need to provide this to ansible ahead of time using one of the available options

* On the command line. Add the argument `--ask-become-pass`

* You can store and retrieve the password using [ansible vault](http://docs.ansible.com/ansible/latest/user_guide/playbooks_vault.html)

See the [ansible documentation about privilege escalation](http://docs.ansible.com/ansible/latest/user_guide/become.html) for details on how to override this behavior to use another method for privilege escalation.

## Run the playbook

### To configure a pg_ha instance

   ```shell
   $ ansible-playbook -i INVENTORY pg_ha.yml
   ```
### To just configure one component (consul, database, etc)

   ```shell
   $ ansible-playbook -i INVENTORY consul.yml
   ```
### To just configure one node

   ```shell
   $ ansible-playbook -i INTENTORY pg_ha.yml -l NODENAME
   ```
